# Ringding

Mockup für Raumgreifende Circle-Packing-Strukturen, zum Beispiel als Messeinstallation. :)
Die Kunststoffringe können nach dem Abbau der Installation als Hoola Hoop Reifen ein zweites Leben beginnen. 

Achtung: Mockup selbst ist nicht sicher B1! Eine nicht-brennbare Variante aus Alurohr und Schlauchschellen wurde erfolgreich getestet, ist aber aufwändiger.
Informationen über die Brandschutzklasse von PE-Kaltwasserrohren konnte ich bisher nicht finden. Tipps dazu sind sehr willkommen!

